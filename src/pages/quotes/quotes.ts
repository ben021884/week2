import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams ,AlertController} from 'ionic-angular';
import { QuotesService } from '../../service/service';

@IonicPage()
@Component({
  selector: 'page-quotes',
  templateUrl: 'quotes.html',
})
export class QuotesPage implements OnInit{
  // parameter buat termia data
    quotes:any;
   
    constructor(public navCtrl: NavController, private navParams:NavParams,
      private quoteservice: QuotesService, private alertCtrl: AlertController)
      {

    }

  // pas halaman di load dia ngelakuin di ngOnInit
  ngOnInit(){
    // .data kalo gak salah dia mua ambil semua
    // .data di pengaruhin sama dari library.html yang navParams & navPush
    this.quotes = this.navParams.data;
    //this.quotes = this.quoteservice.getAllFavoriteQuotes();

    console.log(this.quotes);

  }
  // buat nyimpen
  addToFavorite(quote){
    
    const alert = this.alertCtrl.create({
      title: 'mantap',
      message:'Are you sure you want to add teh quote for favorites?',
      buttons:[
        {
          text:'YES',
          handler:()=>{
            this.quoteservice.addQuoteToFavorites(quote)
            console.log(this.quoteservice);
            this.isfav(quote);
            //this.isfav(quote);
            // this.quoteservice.isFavorite(quote.id);
          }
        },
        {
          text:'NO',
          role:'cancle',
          handler:()=>{
            console.log("NOOOOO")
          }
        }
      ]
    });
    alert.present();
    this.onShowAlert(quote);
  
  }
  // allert buat fav
  onShowAlert(quote) {
    
  }
  isfav(quote){
  //console.log(this.quoteservice);
   return this.quoteservice.isFavorite(quote);
   
  }

  removeFromFavorite(quote){
    const alert = this.alertCtrl.create({
      title: 'mantap',
      message:'Are you sure you want to unfavorite?',
      buttons:[
        {
          text:'YES',
          handler:()=>{
            // isi di sini
            this.quoteservice.removeQuoteFromFavorites(quote);
            console.log(this.quoteservice);
          }
        },
        {
          text:'NO',
          role:'cancle',
          handler:()=>{
            console.log("NOOOOO")
          }
        }
      ]
    });
    alert.present();
    this.onShowAlert(quote);
  }
  
  
}
