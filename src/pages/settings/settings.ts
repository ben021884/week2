import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Toggle } from 'ionic-angular';
import { SettingsService } from '../../service/settings';


@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private setService: SettingsService ) {
  }

  onToggle(toggle : Toggle){
    // pake service baru
    this.setService.setBackground(toggle.value);
    console.log(toggle.value);
  }

  isChecked(){
    
    return this.setService.isAltBackground();
  }

}
