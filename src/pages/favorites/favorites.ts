import { Component, } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { QuotesService } from '../../service/service';
import { QuotePage } from'../quote/quote';
import { Quote } from '../../data/quote.interface';
import { SettingsService } from '../../service/settings';

/**
 * Generated class for the FavoritesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-favorites',
  templateUrl: 'favorites.html',
})
export class FavoritesPage {
  quotes:Quote[];
  constructor(public navCtrl: NavController,
    public navParams: NavParams, 
    private quotesService: QuotesService,
    private modalController: ModalController,
    private setService: SettingsService) {
  }

  // function yang lnagsung load pas kalo enter
  ionViewWillEnter(){
    this.quotes = this.quotesService.getAllFavoriteQuotes();
  }
  showQuote(quote : Quote){
    console.log(quote);
    // .create(PAGE_MANA_YANG_MAU_DITAMPILIN)
     let modal = this.modalController.create(QuotePage,{quoteText : quote.text ,quoteAutor: quote.person});
     modal.present();

     modal.onWillDismiss(data=>{
       this.ionViewWillEnter();
     })
  }
  unFavQuote(quote){
    this.quotesService.removeQuoteFromFavorites(quote);
    this.ionViewWillEnter();
  }
  setBgColor(){
    // kalo true dia berubah jadi 'back_fav' kalo false dia jadi ''
    return this.setService.isAltBackground() ? 'back_fav_true' :'back_fav_false';
  }

}
