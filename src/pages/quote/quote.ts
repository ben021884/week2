import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { QuotesService } from '../../service/service';
//import { Quote } from '../../data/quote.interface';

/**
 * Generated class for the QuotePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-quote',
  templateUrl: 'quote.html',
})
export class QuotePage {
  quoteText:string;
  quoteAutor:string;
  quote:any[];
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private viewController :ViewController,
    private quoteService : QuotesService
    ) {
  }

  ionViewDidLoad() {
    this.quoteText=this.navParams.get('quoteText');
    this.quoteAutor = this.navParams.get('quoteAutor');
    console.log(this.quoteAutor);
  }
  closeModal(){
    // modal di aksesnya lewat viewCOntroler jadi kalo mau nutup juga harus pake viewControler
    // function buat nutup modal
    this.viewController.dismiss();

    
  }
  unFavQuote(quote){
    this.quoteService.removeQuoteFromFavorites(quote);
    this.closeModal();
  }

}
