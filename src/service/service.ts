import { Quote } from '../data/quote.interface';
import { indexDebugNode } from '@angular/core/src/debug/debug_node';

export class QuotesService {

    private favoriteQuotes: Quote[] = [];

    addQuoteToFavorites(quote: Quote) {
        this.favoriteQuotes.push(quote);
    }

    removeQuoteFromFavorites(quote: Quote) {
        let quote_idx = this.favoriteQuotes.indexOf(quote);
         this.favoriteQuotes.splice(quote_idx,1);
    }

    isFavorite(quote: Quote) {
        return this.favoriteQuotes.some(quotess =>quotess.id === quote.id);
    }

    getAllFavoriteQuotes(){
        return this.favoriteQuotes.slice();
    }
}
// push=buat masukin ke array index terakhir
// pop = delet idex terakhir
// slice = ngecopy aray ke var lain
// splice = ngedelet indexDebugNode(idex_keberapa,JUMLAHNYA) 